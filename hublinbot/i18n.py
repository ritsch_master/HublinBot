# -*- coding: utf-8 -*-
################################################################################
# This file is part of HublinBot.
#
# Copyright 2018 Richard Paul Baeck <richard.baeck@free-your-pc.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE. 
################################################################################
'''
Created on Sep 02, 2018

@author: Richard Bäck
'''

import gettext

class I18N():
    def __init__(self, domain):
        self.translations = { }
    
        self.translations['en'] = True
        self.translations['de'] = True
        
        localedir = None
        localedir = 'locale'
        
        self.translations['de'] = gettext.translation(domain,
                                                      localedir = localedir,
                                                      languages = ['de'])
        self.translations['en'] = gettext.translation(domain, 
                                                      localedir = localedir,
                                                      languages = ['en'])
        
    
    def translate(self, text, language = 'en'):
        translation = self.translations[language]
      
        translated = '' 
        if translation is not None: 
            translated = translation.gettext(text)
            
        return translated
    
