# -*- coding: utf-8 -*-
################################################################################
# This file is part of HublinBot.
#
# Copyright 2018 Richard Paul Baeck <richard.baeck@free-your-pc.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE. 
################################################################################
'''
Created on Aug 29, 2018

@author: Richard Bäck
'''

from telegram.ext import Updater

from . import CONFIG_SINGLETON
from . import I18N
from . import StartCommand
from . import HublinCommand
from . import HublinSimpleCommand
from . import UnknownCommand

class HublinDispatcher():
    def __init__(self, i18n : I18N, updater : Updater = None):
        self.updater = None
        self.i18n = None
      
        if updater == None:  
            self.updater = Updater(token = CONFIG_SINGLETON.get_config()["Token"])

        self.i18n = i18n
        
        self.init_commands(i18n, self.updater)
        
    def init_commands(self, i18n : I18N, updater : Updater):
        start_command = StartCommand(i18n)
        start_command.register(updater.dispatcher)

        hublin_command = HublinCommand(i18n)
        hublin_command.register(updater.dispatcher)
   
        hublin_simple_command = HublinSimpleCommand(i18n)
        hublin_simple_command.register(updater.dispatcher)

        unknown_command = UnknownCommand(i18n)
        unknown_command.register(updater.dispatcher)

    
    def run(self):
        self.updater.start_polling()
    
    
    def idle(self):
        self.updater.idle()
       
        
    def stop(self):
        self.updater.stop()
        
        