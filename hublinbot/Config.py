# -*- coding: utf-8 -*-
################################################################################
# This file is part of HublinBot.
#
# Copyright 2018 Richard Paul Baeck <richard.baeck@free-your-pc.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE. 
################################################################################
'''
Created on Aug 29, 2018

@author: Richard Bäck
'''

from enum import Enum
import sys
import os
import copy

from yaml import load, dump
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

class FileMode(Enum):
    DEVELOPMENT = 1
    PRODUCTION_USER = 2
    PRODUCTION_LOCAL = 3
    PRODUCTION_SYSTEM = 4

class Config():
    PATH_TRIES = [ ('.', FileMode.DEVELOPMENT),
                   ('/usr/local/share/hublinbot', FileMode.PRODUCTION_LOCAL),
                   ('/usr/share/hublinbot', FileMode.PRODUCTION_SYSTEM) ]
    

    def __init__(self):
        self.config = None
        
        config_filename = self.get_absolute_path('config.yaml')
        
        with open(config_filename, 'r') as file:
            self.config = load(file, Loader = Loader)
     
        
    def try_paths(self, basename):
        """
        Returns a tuple with:
        1. The absolute filename for the basename
        2. The FileMode
        """
        for dirname in Config.PATH_TRIES:
            filename = dirname[0] + '/' + basename
            if os.path.isfile(filename):
                return (filename, dirname[1])
           
            
    def get_absolute_path(self, basename):
        return self.try_paths(basename)[0]
   
    
    def get_file_mode(self, basename):
        return self.try_paths(basename)[1] 
   
    
    def get_try_paths_dirname(self, file_mode : FileMode):
        for dirname in Config.PATH_TRIES:
            if dirname[1] == file_mode:
                return dirname[0]
            
            
    def get_config(self):
        return self.config


CONFIG_SINGLETON = Config()
