# -*- coding: utf-8 -*-
################################################################################
# This file is part of HublinBot.
#
# Copyright 2018 Richard Paul Baeck <richard.baeck@free-your-pc.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE. 
################################################################################
'''
Created on Aug 29, 2018

@author: Richard Bäck
'''

import random
import uuid
import logging

from telegram.ext import CommandHandler
from telegram.ext import Dispatcher
from telegram.ext import MessageHandler
from telegram.ext import Filters
from telegram.ext.dispatcher import run_async

from . import Config 
from . import I18N

def get_language_code(update):
    language_code = update.message.from_user.language_code.split('-')[0]
    return language_code


class Command():
    def get_command(self) -> str:
        pass
    
    def run(self, bot, update):
        pass
    
    
    def register(self, dispatcher : Dispatcher):
        handler = CommandHandler(self.get_command(), self.run) 
        dispatcher.add_handler(handler) 


class StartCommand(Command):
    def __init__(self, i18n : I18N):
        self.i18n = None

        self.i18n = i18n
   
    
    def get_command(self):
        return "start"

    @run_async
    def run(self, bot, update):
        text = self.get_i18n().translate("Hello! Please use /hublin to get a new room.",
                                         get_language_code(update))
        bot.send_message(chat_id = update.message.chat_id, 
                         text = text)
        
    
    def get_i18n(self):
        return self.i18n


class UnknownCommand(Command):
    def __init__(self, i18n : I18N):
        self.i18n = None

        self.i18n = i18n
   
    
    def get_command(self):
        return None

  
    @run_async
    def run(self, bot, update):
        text = self.get_i18n().translate("Sorry, I didn't understand that command.",
                                         get_language_code(update))
        bot.send_message(chat_id = update.message.chat_id, 
                         text = text)


    def register(self, dispatcher : Dispatcher):
        handler = MessageHandler(Filters.command, self.run)
        dispatcher.add_handler(handler) 
        
        
    def get_i18n(self):
        return self.i18n
    
    
class HublinCommand(Command):
    def __init__(self, i18n : I18N):
        self.i18n = None
        self.base_url = None

        self.i18n = i18n
        self.base_url = 'https://hubl.in/'

    
    def get_command(self) -> str:
        return "hublin"


    def get_meeting_id(self) -> str:
        guid = uuid.uuid4()
        
        return str(guid)
    

    @run_async
    def run(self, bot, update):
        path = self.get_meeting_id()
        
        full_url = self.get_base_url() + path
        
        base_text = self.get_i18n().translate("Your meeting: ",
                                              get_language_code(update))
        text = base_text + full_url
        bot.send_message(chat_id = update.message.chat_id, 
                         text = text)


    def get_base_url(self):
        return self.base_url
    

    def get_i18n(self):
        return self.i18n
    
    
class HublinSimpleCommand(HublinCommand):
    def __init__(self, i18n : I18N):
        HublinCommand.__init__(self, i18n)
        
        self.separator = None
        self.word_count = None
        self.word_list = None
        
        self.separator = '-'
        self.word_count = 3
        self.init_word_list()
        

    def init_word_list(self):
        filename = Config.CONFIG_SINGLETON.get_absolute_path('words.txt')
        
        word_list = ''
        with open(filename, 'r') as file:
            word_list = file.read() 
            word_list = word_list.split('\n')
            
        self.word_list = word_list
        
   
    def get_command(self) -> str:
        return "hublinsimple" 
   
    
    def get_meeting_id(self) -> str:
        url_word_list = ''
        for i in range(self.get_word_count()):
            index = random.randint(1, len(self.get_word_list()))
            word = self.get_word_list()[index]
             
            if i > 0:
                url_word_list = url_word_list + self.get_separator() 
            url_word_list = url_word_list + word
      
        return url_word_list
    
    
    def get_separator(self):
        return self.separator
    

    def get_word_count(self):
        return self.word_count
   
    
    def get_word_list(self):
        return self.word_list
    
    
 