# HublinBot

This is a Telegram Bot to supply room URLs for (Hubl.in)[https://hubl.in].


## Author

Richard Bäck (richard.baeck@free-your-pc.com)


## License

MIT License