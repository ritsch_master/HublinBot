################################################################################
# This file is part of HublinBot.
#
# Copyright 2018 Richard Paul Baeck <richard.baeck@free-your-pc.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE. 
################################################################################

PYTHON=python3
PY2DSC=py2dsc

LANG_DIR=po

PROJECT=HublinBot
MAINTAINER=Richard Paul Baeck <richard.baeck@free-your-pc.com>

PY2DSC_WITH_PYTHON2=False
PY2DSC_WITH_PYTHON3=True

DEBIAN_PYTHON=python3
DEBIAN_DEPENDENCIES=

all: update-po
	mkdir -p locale/de/LC_MESSAGES
	msgfmt $(LANG_DIR)/de.po -o locale/de/LC_MESSAGES/$(PROJECT).mo
	mkdir -p locale/en/LC_MESSAGES
	msgfmt $(LANG_DIR)/en.po -o locale/en/LC_MESSAGES/$(PROJECT).mo


dist: all
	$(PYTHON) setup.py sdist --formats=gztar

update-po:
	 msgmerge -U $(LANG_DIR)/de.po $(LANG_DIR)/$(PROJECT).pot
	 msgmerge -U $(LANG_DIR)/en.po $(LANG_DIR)/$(PROJECT).pot


clean:
	rm -rf locale


distclean: clean
	rm -rf MANIFEST
	rm -rf dist/
	rm -rf build/

