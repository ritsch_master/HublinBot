# -*- coding: utf-8 -*-
################################################################################
# This file is part of HublinBot.
#
# Copyright 2018 Richard Paul Baeck <richard.baeck@free-your-pc.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE. 
################################################################################
'''
Created on Aug 30, 2018

@author: Richard Bäck
'''

import unittest

from telegram.ext import CommandHandler
from telegram.ext import Filters
from telegram.ext import MessageHandler
from telegram.ext import Updater
from ptbtest import ChatGenerator
from ptbtest import MessageGenerator
from ptbtest import Mockbot
from ptbtest import UserGenerator

from hublinbot import HublinDispatcher

class HublinDispatcherTest(unittest.TestCase):
    def setUp(self):
        self.bot = Mockbot()
        self.user_generator = UserGenerator()
        self.chat_generator = ChatGenerator()
        self.message_generator = MessageGenerator(self.bot)
#         self.updater = Updater(bot = self.bot)
#         
#         self.hublin_dispatcher = HublinDispatcher(updater = self.updater)
#         self.updater.start_polling()
        
    
    def tearDown(self):
        pass
#         self.updater.stop()
  
        
    def test_start(self):
        pass
#         user = self.user_generator.get_user(first_name = "Test", 
#                                             last_name = "User")
#         chat = self.chat_generator.get_chat(user = user) 
#         update = self.message_generator.get_message(user = user, 
#                                                     chat = chat, 
#                                                     text = "/start") 
#         self.bot.insertUpdate(update) 
#        
#         self.assertEqual(len(self.bot.sent_messages), 1)
#         sent = self.bot.sent_messages[0] 
#         self.assertEqual("Hello! Please use /hublin to get a new room.", sent['text'],
#                          "Answer to /start is wrong!")
       
        
if __name__ == '__main__':
    unittest.main()
    
    
